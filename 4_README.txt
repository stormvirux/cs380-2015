=====================================================================
CS380 GPU and GPGPU Programming, KAUST
Programming Assignment #4
Conjugate Gradient and Image Deblurring

Contacts: 
peter.rautek@kaust.edu.sa
ronell.sicat@kaust.edu.sa
=====================================================================

Task: 

1. Conjugate Gradient Linear Systems Solver
Implement the conjugate gradient (CG) method:
http://en.wikipedia.org/wiki/Conjugate_gradient_method
using CUDA.
The conjugate gradient method iteratively solves the linear system
Ax = b
The skeleton of the algorithm is already provided. You have to program the matrix and vector operations that are executed on the GPU.
For testing a small hardcoded 4x4 matrix, and .txt files containing sparse matrices are provided. 

To reach maximum points you have to use shared memory for the vector in the matrix-vector-multiplication and perform vector reduction using shared memory as well, as discussed in the lecture!

You can get bonus points for additional performance optimizations, i.e., taking advantage of the sparseness of the matrix and/or padding.


2. Image Deblurring
Once the CG method is working for the matrices it can be used to solve practical applications.
Implement a simple image deblurring application that works for a known blurring filter.
A image is loaded and blurred with a filter kernel.
We can deblur the image again by solving the equation 
Ax = b
were x is the unknown input image that was filtered with filter operation A such that the result is the known blurred image b.
In order to formulate the blurring operation (convolution) as a matrix multiplication b and x are represented as vectors and each row of matrix A is one filter kernel for the whole image x.
This naive method is very memory inefficient (O(N^2) where N is the number of pixels). For bonus points implement a method that is more efficient in terms of memory and/or performance.
For instance you can modify the code to divide the image into small patches and solve for them separately.