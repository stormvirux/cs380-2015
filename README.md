# CS380 - GPGPU Programming Framework

## Introduction

This is the framework for the 'CS380 - GPGPU Programming' lab. 
If you are a student you should fork this repo and work on your copy of the repo.
Each assignment will have its own folder that contains a stand-alone program.
You should commit your solution and the report to the assignment folder.

## Compiling and Running the Framework

The framework is tested on Windows machines with Nvidia graphic cards. 
Install the newest version of CUDA and update your graphics driver.
A solution file is provided for Visual Studio 2010.
If you want to use a different operating system or IDE, you will have to port parts of the code yourself.
The framework uses the GLEW and GLFW libraries for setting up a window and an OpenGL context. 
Both libraries are included in the framework and statically linked. 

## Contact
Peter Rautek: peter.rautek@kaust.edu.sa
Ronell Sicat: ronell.sicat@kaust.edu.sa
