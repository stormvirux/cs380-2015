// CS 380 - GPGPU Programming - KAUST
//
// Programming Assignment #2

// system includes
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <math.h>

// library includes
// include glm: http://glm.g-truc.net/
#include <glm/glm.hpp>

// include GLEW: http://glew.sourceforge.net/
#define GLEW_STATIC // including glew as static library
#include <GL/glew.h> 

// include glfw library: http://www.glfw.org/
#define GLFW_INCLUDE_GLCOREARB // defines that opengl core profile is used 
#include <GLFW/glfw3.h>

// includes for CUDA
#include <cuda_runtime.h>

// framework includes
#include "glutils.h"
#include "vbocube.h"
#include "vbomesh.h"

// pointer to the window that will be created by glfw
GLFWwindow* m_pWindow;

// window size
const unsigned int m_uWindowWidth = 512;
const unsigned int m_uWindowHeight = 512;

// a simple cube
VBOCube *m_pCube;

// a more complex mesh
VBOMesh *m_pMesh; 


// glfw errors are reported to this callback
static void errorCallback(int error, const char* description)
{
	fputs(description, stderr);
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	// TODO: implement key mappings (rendering parameters, camera and object manipulation)
}


static void cursorPosCallback(GLFWwindow* window, double x, double y)
{
	// TODO: implement camera/object transformation (at least rotation and zooming for camera, translation for objects)
}

static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	// TODO: put further mouse button handling here
}

static void scrollCallback(GLFWwindow* window, double dx, double dy)
{
	// OPTIONAL: put further scroll handling here
}


// render a frame
void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// TODO: use your program
	// TODO: set uniforms
	
	m_pCube->render();

	// TODO: once you are done setting up the basic rendering you can load and render more complex meshes! 
	// Try loading and rendering the m_pMesh
	//m_pMesh->render();
	
}

// query GPU functionality we need for OpenGL, return false when not available
bool queryGPUCapabilitiesOpenGL()
{
	// TODO: Check for required OpenGL functionality
	return true;
}

// query GPU functionality we need for CUDA, return false when not available
bool queryGPUCapabilitiesCUDA()
{
	return true;
}


// check for opengl error and report if any
void checkForOpenGLError()
{
	GLenum error = glGetError();
	if (error==GL_NO_ERROR) {
		return;
	}
	std::string errorString = "unknown";
	std::string errorDescription = "undefined";

	if (error==GL_INVALID_ENUM)
	{
		errorString = "GL_INVALID_ENUM";
		errorDescription = "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_VALUE)
	{
		errorString = "GL_INVALID_VALUE";
		errorDescription = "A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_OPERATION)
	{
		errorString = "GL_INVALID_OPERATION";
		errorDescription = "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_FRAMEBUFFER_OPERATION)
	{
		errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
		errorDescription = "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_OUT_OF_MEMORY)
	{
		errorString = "GL_OUT_OF_MEMORY";
		errorDescription = "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
	}
	else if (error==GL_STACK_UNDERFLOW)
	{
		errorString = "GL_STACK_UNDERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to underflow.";
	}
	else if (error==GL_STACK_OVERFLOW)
	{
		errorString = "GL_STACK_OVERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to overflow.";
	}

	fprintf(stdout, "Error %s: %s\n", errorString.c_str(), errorDescription.c_str());
}

				  
static void debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam);


// init glfw and opengl 
bool initGL(int argc, char **argv)
{
	glfwSetErrorCallback(errorCallback);

	// initialize the glfw library
	if (!glfwInit())
		return false;

	// set window hints
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	m_pWindow = glfwCreateWindow(m_uWindowWidth, m_uWindowHeight, "CS380 - OpenGL", NULL, NULL); // Windowed
	// window = glfwCreateWindow(800, 600, "CS380 - OpenGL", glfwGetPrimaryMonitor(), nullptr); // Fullscreen

	if (!m_pWindow)
	{
		glfwTerminate();
		return false;
	}

	// make the window's context current 
	glfwMakeContextCurrent(m_pWindow);

	// callback functions for input events (mouse, keyboard). 
	// see glfw documentation for more information:
	// http://www.glfw.org/docs/latest/input.html
	glfwSetKeyCallback(m_pWindow, keyCallback);
	glfwSetCursorPosCallback(m_pWindow, cursorPosCallback);
	glfwSetMouseButtonCallback(m_pWindow, mouseButtonCallback);
	glfwSetScrollCallback(m_pWindow, scrollCallback);
	
	checkForOpenGLError();

	// init glew 
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		// glewInit failed, something is seriously wrong
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		return false;
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	// there is a bug with glew 1.10 (as reported at http://www.opengl.org/wiki/OpenGL_Loading_Library): init gives a GL_INVALID_ENUM error when using the opengl core profile. 
	// nothing to worry about when glewExperimental is set to GL_TRUE
	// however, we check for OpenGL errors here and simply ignore this one.
	GLenum error = glGetError();
	
	return true;
}
 
// draws and handles input events until window is closed
void programMainLoop()
{
	// one time initializations
	// set clear color 
	glClearColor(0.0, 0.0, 0.25, 1.0);
	
	// loop until the user closes the window
	while (!glfwWindowShouldClose(m_pWindow))
	{
		// render a frame
		render();

		// check for opengl errors
		checkForOpenGLError();
		
		// display: swap front and back buffers 
		glfwSwapBuffers(m_pWindow);

		// poll for events
		glfwPollEvents();
	}

	glfwTerminate();

}

void setupScene()
{
	// TODO: Set up a camera. Hint: The glm library is your friend
	// TODO: Set up glsl program (at least vertex and fragment shaders). Hint: The GLSLProgram class is your friend!

	m_pCube = new VBOCube();
	
	// TODO: once you are done setting up the basic rendering you can load and render more complex meshes!
	// Try loading and rendering the m_pMesh
	// m_pMesh = new VBOMesh("data/bs_ears.obj",false,true,true);
}



// entry point
int main(int argc, char** argv)
{
	// init opengl
	if (!initGL(argc, argv)) 
	{
		// quit in case initialization fails
		return -1;
	}

	// query opengl capabilities
	if (!queryGPUCapabilitiesOpenGL()) 
	{
		// quit in case capabilities are insufficient
		return -1;
	}

	// query cuda capabilities
	if(!queryGPUCapabilitiesCUDA())
	{
		// quit in case capabilities are insufficient
		return -1;
	}

	setupScene();

	// start traversing the main loop
	programMainLoop();
}

