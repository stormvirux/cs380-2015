#ifndef PNG___H
#define PNG___H

#pragma once
// PNG loading and saving code

#include <string>

using namespace std;

namespace PNG {

#define PNG_DEBUG 3
#include "libpng/png.h"

    Image load(string filename) {
        png_byte header[8];	// 8 is the maximum size that can be checked
        png_structp png_ptr = NULL;
        png_infop info_ptr = NULL;
        int number_of_passes;
        png_bytep * row_pointers;
    
        /* open file and test for it being a png */
        FILE *f = fopen( filename.c_str(), "rb");
        myassert( ( f != NULL ), "File %s could not be opened for reading\n", filename.c_str());
        fread(header, 1, 8, f);
        myassert(!png_sig_cmp(header, 0, 8), "File %s is not recognized as a PNG file\n", filename.c_str());
    
        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    
        myassert( ( png_ptr != NULL ), "[read_png_file] png_create_read_struct failed\n");
    
        info_ptr = png_create_info_struct(png_ptr);
        myassert( ( info_ptr != NULL ), "[read_png_file] png_create_info_struct failed\n");
    
        myassert(!setjmp(png_jmpbuf(png_ptr)), "[read_png_file] Error during init_io\n");
    
        png_init_io(png_ptr, f);
        png_set_sig_bytes(png_ptr, 8);
    
        png_read_info(png_ptr, info_ptr);
    
	int width = png_get_image_width(png_ptr, info_ptr);
	int height = png_get_image_height(png_ptr, info_ptr);
	int channels = png_get_channels(png_ptr, info_ptr);
	int bit_depth = png_get_bit_depth(png_ptr, info_ptr);

	// Expand low-bpp images to have only 1 pixel per byte (As opposed to tight packing)
	if (bit_depth < 8)
	    png_set_packing(png_ptr);

	Image im(1, width, height, channels);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);
    
        // read the file
        myassert(!setjmp(png_jmpbuf(png_ptr)), "[read_png_file] Error during read_image\n");
    
        row_pointers = new png_bytep[im.height];
        for (int y = 0; y < im.height; y++)
            //row_pointers[y] = new png_byte[info_ptr->rowbytes];
			row_pointers[y] = new png_byte[ png_get_rowbytes( png_ptr, info_ptr) ];

        png_read_image(png_ptr, row_pointers);
    
        fclose(f);
    
        // convert the data to floats
	if (bit_depth <= 8) {
	    int bit_scale = 8/bit_depth;
	    for (int y = 0; y < im.height; y++) {
		png_bytep srcPtr = row_pointers[y];
		for (int x = 0; x < im.width; x++) {
		    for (int c = 0; c < im.channels; c++) {
			im(x, y)[c] = LDRtoHDR(bit_scale* (*srcPtr++) );
		    }
		}
	    }
	} else if (bit_depth == 16) {
	    printf("Reading a 16-bit PNG image (Image may be darker than expected!)\n");
	    for (int y = 0; y < im.height; y++) {
		png_bytep srcPtr = row_pointers[y];
		for (int x = 0; x < im.width; x++) {
		    for (int c = 0; c < im.channels; c++) {
			im(x, y)[c] = LDR16toHDR(*srcPtr);  // Note: Endian issues may be possible here, seems to work in WinXP
			srcPtr+=2;					       
		    }
		}
	    }
	}
    
        // clean up
        for (int y = 0; y < im.height; y++)
            free(row_pointers[y]);
        free(row_pointers);
    
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	return im;
    }
    

    void save(Window im, string filename) {
        png_structp png_ptr = NULL;
        png_infop info_ptr = NULL;
        png_bytep * row_pointers;
        png_byte color_type;

        myassert(im.frames == 1, "Can't save a multi-frame PNG image\n");
        myassert(im.channels > 0 && im.channels < 5, 
               "Imagestack can't write PNG files that have other than 1, 2, 3, or 4 channels\n");

        png_byte color_types[4] = {PNG_COLOR_TYPE_GRAY, PNG_COLOR_TYPE_GRAY_ALPHA, 
                                   PNG_COLOR_TYPE_RGB,  PNG_COLOR_TYPE_RGB_ALPHA};
        color_type = color_types[im.channels - 1];
    
        // open file
        FILE *f = fopen( filename.c_str(), "wb");
        myassert( ( f != NULL ), "[write_png_file] File %s could not be opened for writing\n", filename.c_str());
    
        // initialize stuff
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        myassert( ( png_ptr != NULL ), "[write_png_file] png_create_write_struct failed\n");
    
        info_ptr = png_create_info_struct(png_ptr);
        myassert( ( info_ptr != NULL ), "[write_png_file] png_create_info_struct failed\n");
    
        myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during init_io\n");
    
        png_init_io(png_ptr, f);
    
        // write header
        myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during writing header\n");
    
        png_set_IHDR(png_ptr, info_ptr, im.width, im.height,
                     8, color_type, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    
        png_write_info(png_ptr, info_ptr);
    
        // convert the floats to bytes
        row_pointers = new png_bytep[im.height];
        for (int y = 0; y < im.height; y++) {
            row_pointers[y] = new png_byte[ png_get_rowbytes( png_ptr, info_ptr) ];
            png_bytep dstPtr = row_pointers[y];
            for (int x = 0; x < im.width; x++) {
                for (int c = 0; c < im.channels; c++) {
                    *dstPtr++ = (png_byte)(HDRtoLDR(im(x, y)[c]));
                }
            }
        }
    
        // write data
        myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during writing bytes");
    
        png_write_image(png_ptr, row_pointers);
    
        // finish write
        myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during end of write");
    
        png_write_end(png_ptr, NULL);
    
        // clean up
        for (int y = 0; y < im.height; y++)
            free(row_pointers[y]);
        free(row_pointers);
    
        fclose(f);
    
        png_destroy_write_struct(&png_ptr, &info_ptr);
    }

	Image convertToGrayScale(Window im, string filename) {
		png_structp png_ptr = NULL;
		png_infop info_ptr = NULL;
		png_bytep * row_pointers;
		png_byte color_type;

		myassert(im.frames == 1, "Can't save a multi-frame PNG image\n");
		myassert(im.channels > 0 && im.channels < 5, 
			"Imagestack can't write PNG files that have other than 1, 2, 3, or 4 channels\n");

		png_byte color_types[4] = {PNG_COLOR_TYPE_GRAY, PNG_COLOR_TYPE_GRAY_ALPHA, 
			PNG_COLOR_TYPE_RGB,  PNG_COLOR_TYPE_RGB_ALPHA};
		color_type = color_types[0];

		// open file
		FILE *f = fopen( filename.c_str(), "wb");
		myassert( ( f != NULL ), "[write_png_file] File %s could not be opened for writing\n", filename.c_str());

		// initialize stuff
		png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		myassert( ( png_ptr != NULL ), "[write_png_file] png_create_write_struct failed\n");

		info_ptr = png_create_info_struct(png_ptr);
		myassert( ( info_ptr != NULL ), "[write_png_file] png_create_info_struct failed\n");

		myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during init_io\n");

		png_init_io(png_ptr, f);

		// write header
		myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during writing header\n");

		png_set_IHDR(png_ptr, info_ptr, im.width, im.height,
			8, color_type, PNG_INTERLACE_NONE,
			PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

		png_write_info(png_ptr, info_ptr);

		Image outputImage(1, im.width, im.height, 1);

		// convert the floats to bytes
		row_pointers = new png_bytep[im.height];
		for (int y = 0; y < im.height; y++) {
			row_pointers[y] = new png_byte[ png_get_rowbytes( png_ptr, info_ptr) ];
			png_bytep dstPtr = row_pointers[y];
			for (int x = 0; x < im.width; x++) {
				// 0.2989 * R + 0.5870 * G + 0.1140 * B
				float luminance = ( 0.2989f * im(x, y)[0] ) + ( 0.5870f * im(x, y)[1] ) + ( 0.114f * im(x, y)[2] );
				outputImage( x, y )[0] = luminance;
				*dstPtr++ = (png_byte)( HDRtoLDR( luminance ) );
			}
		}

		// write data
		myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during writing bytes");

		png_write_image(png_ptr, row_pointers);

		// finish write
		myassert(!setjmp(png_jmpbuf(png_ptr)), "[write_png_file] Error during end of write");

		png_write_end(png_ptr, NULL);

		// clean up
		for (int y = 0; y < im.height; y++)
			free(row_pointers[y]);
		free(row_pointers);

		fclose(f);

		png_destroy_write_struct(&png_ptr, &info_ptr);

		return outputImage;
	}
}

#endif
